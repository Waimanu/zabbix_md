# update older templates automatically, from the latest template

LATEST = template_md_5.2.xml

all: \
	dist/zabbix_template_md_5.2.zip

dist:
	mkdir dist

dist/zabbix_template_md_5.2.zip: dist
	zip -r dist/zabbix_template_md.zip LICENSE README.md template_md.json template_md.xml userparameter_md.conf

clean:
	rm -vfr \
		dist

.PHONY: all clean
